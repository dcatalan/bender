package src;

/**
 * Created by dcatalans on 10/03/16.
 */

class Bender {
    //Variables membre
    private final String map;
    private final int linea_h;
    private final int linea_v;
    private final char [][] graella;
    private int punt_x;
    private int punt_y;
    private boolean invertit = false;

    //Constructor
    public Bender(String map) {
        this.map = map;
        this.linea_h = contar_caracter_horizontal(map);
        this.linea_v =contar_caracter_vertical(map);
        this.graella = crearArray();

    }
    //Funcio que calcula el ample del mapa.
    private int contar_caracter_horizontal(String mapa) {
        int c_width = 0;
        for (int i = 0; mapa.charAt(i) != '\n'; i++) {
            c_width++;
        }
        return c_width;
    }
    //Funcio que calcula el llarg del mapa
    private int contar_caracter_vertical(String mapa){
        int c_heigth = 0;
        for (int i = 0; i < mapa.length(); i++) {
            if (mapa.charAt(i) == '\n') {
                c_heigth ++;
            }
        }
        c_heigth++;
        return c_heigth;
    }

    //Amb el llarg i l'ample del mapa, pasam el mapa a array bidimensional.
    private char[][] crearArray() {
        int separacio = 0;
        char[][] ar = new char[linea_v][linea_h];

        for (int i = 0; i<linea_v; i++) {
            int o = 0;
            for (int j = 0; o<linea_h;j++) {
                if (map.charAt(j+separacio) == '\n') {
                    continue;
                }
                else {
                    ar[i][o] = map.charAt(j + separacio);
                    //Treim el valor inicial de X
                    if(map.charAt(j+separacio) == 'X' || map.charAt(j+separacio) == 'x') {
                        this.punt_y = i;
                        this.punt_x = o;
                    }
                    o++;
                }
            }
            separacio += linea_h;
            if(i<linea_v-1) {
                separacio += 1;
            }
        }
        return ar;
    }
    //Canvia la posicio del robot segons el que reb per parametre.
    private void moverRobot(char coord) {
        switch (coord) {
            case 'S':
                punt_y +=1;
                break;
            case 'N':
                punt_y -=1;
                break;
            case 'W':
                punt_x-=1;
                break;
            case 'E':
                punt_x+=1;
                break;

        }
    }
    //Calcula el area (espais en buit a l'interior del mapa).
    private int area () {
        int area = 0;
        for (int i = 0; i < linea_v; i++) {
            for (int j = 0; j < linea_h ; j++) {
               if (graella[i][j] == ' ') {
                   area++;
               }
            }
        }
        return area;
    }
    //Funcio que mostra l'array del mapa en forma de string.
    @Override
    public String toString() {
        String resultado = "";
        for (int i = 0; i < linea_v; i++) {
            for (int j = 0; j < linea_h ; j++) {
                resultado += graella[i][j];
            }
            resultado += '\n';
        }
        return resultado;
    }
    //Funcio principal del programa Bender.
    public String run(){
        //Variables
        int area = area();
        String resultado = "";
        boolean dolar = false;
        int pasos = 0;

        //Mentres que no trobi el dolar (meta final).
        while(!dolar) {
            //Declaracio de variables.
            char dir = direccio();
            resultado += dir;
            char actual = graella[punt_y][punt_x];

            //En cas de que sigui bucle infinit.
            if (pasos > area){return "Es un bucle!";}

            //En cas de que la posicio del robot actual sigui espai
            if(actual == ' ') {
                //Mentres continui sent espai..
                while (actual == ' ') {
                    //Continuarà fins que la seguent pasa sigui un #
                    if (seguent(dir) == '#') {
                        break;
                    }
                    resultado += dir;
                    moverRobot(dir);
                    actual = graella[punt_y][punt_x];

                    //Si actual es torna una T, llavors el teleporta a l'altra T i avança.
                    if( actual == 'T') {
                        trobarT();
                        moverRobot(dir);
                        resultado+=dir;
                        actual = graella[punt_y][punt_x];
                    }
                    pasos++;
                }
            }
            //En cas de que sigui una t, teleporta el robot
            if( actual == 'T') {
                trobarT();
                if(seguent(dir) == '#'){
                    continue;
                }
                moverRobot(dir);
                resultado+=dir;
                actual = graella[punt_y][punt_x];
            }
            //Si actual esta en la meta, rompem el bucle i finalitzarà el programa.
            if(actual == '$') {
                break;
            }
            //Si actual trobam una I, al, moment de accedir mètrode direcció,
            //cercarà el valor invertit o no correcte per la ocasió.
            if(actual == 'I') {
                invertit = ! invertit;
                continue;
            }
            pasos++;

        }
        return resultado;
    }
    //Funcio que troba el pass seguent que farà el Robot.
    private char seguent(char dir) {
        char seguent = '\0';
        switch (dir) {
            case 'S':
                seguent = graella[punt_y + 1][punt_x];
                break;
            case 'E':
                seguent = graella[punt_y][punt_x + 1];
                break;
            case 'N':
                seguent = graella[punt_y - 1][punt_x];
                break;
            case 'W':
                seguent = graella[punt_y][punt_x - 1];
        }
        return seguent;
    }

    //Funció que troba la T diferent a la que hi hauria en actual i teleporta el robot.
    private void trobarT() {
        for(int i = 0;i<linea_v; i++) {
            for(int j =0; j<linea_h; j++) {
                if(graella[i][j] == 'T' && i != punt_y && j != punt_x ) {
                    punt_y = i;
                    punt_x = j;
                    return;
                }
            }
        }
    }
    //Funcio que calcula on s' ha de moure el robot segons els seus obstacles.
    private char direccio() {
        char dir = '\0';

        if (!invertit) {
            if (graella[punt_y + 1][punt_x] != '#') {
                moverRobot('S');
                dir = 'S';
            } else if (graella[punt_y][punt_x + 1] != '#') {
                moverRobot('E');
                dir = 'E';
            } else if (graella[punt_y - 1][punt_x] != '#') {
                moverRobot('N');
                dir = 'N';
            } else if (graella[punt_y][punt_x - 1] != '#') {
                dir = 'W';
                moverRobot('W');
            }
        }
        if (invertit) {
            if (graella[punt_y - 1][punt_x] != '#') {
                moverRobot('N');
                dir = 'N';
            }
            else if (graella[punt_y][punt_x - 1] != '#') {
                dir = 'W';
                moverRobot('W');
            }
            else if (graella[punt_y + 1][punt_x] != '#') {
                moverRobot('S');
                dir = 'S';
            }
            else if (graella[punt_y][punt_x + 1] != '#') {
                moverRobot('E');
                dir = 'E';
            }
        }
        return dir;
    }

}